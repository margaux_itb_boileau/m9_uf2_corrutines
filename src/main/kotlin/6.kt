/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 6
*/

import kotlinx.coroutines.*

fun main() {
    println("Inici cursa")
    runBlocking {
        GlobalScope.launch { cavall("Cavall 1") }
        GlobalScope.launch { cavall("Cavall 2") }
        delay(20000)
        println("Final cursa")
    }
}

suspend fun cavall(nomCavall:String) = coroutineScope {
    launch{
        for (i in 1..4) {
            val delay = (1000L..5000L).random()
            println("$nomCavall en punt: $i")
            delay(delay)
        }
        println("Meta creuada")
    }
}