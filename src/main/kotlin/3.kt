/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 3
*/

import kotlinx.coroutines.*
import java.io.File
import kotlin.system.measureTimeMillis

fun main() {

    val time = measureTimeMillis {
        val file = File("./src/main/kotlin/song")
        runBlocking {
            printLines(file.readLines())
        }
    }
    println(time)
}


suspend fun printLines(lines:List<String>) {
    for (line in lines) {
        coroutineScope {
            launch {
                println(line)
                delay(3000)
            }
        }
    }
}