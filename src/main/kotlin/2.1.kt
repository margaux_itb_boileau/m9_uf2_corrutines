/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 2.1
*/

import kotlinx.coroutines.*

fun main() {
    runBlocking {
        launch {
            printHello1(3)
        }
    }
    println("Finished!")
}

suspend fun printHello1(times:Int) {
    for (i in 1..times) {
        println("Message $i: Hello World!")
        delay(1000)
    }
}