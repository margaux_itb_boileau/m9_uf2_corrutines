/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 4
*/

import kotlinx.coroutines.*
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val randomNum = (1..50).random()
    println("You have 10 seconds to guess the secret number...")
    GlobalScope.launch {
        do {
            println("Enter a number:")
            val guess = scanner.nextInt()
            if(guess==randomNum) println("You got it!")
            else println("Wrong number!")
        } while (true)
    }
    runBlocking {
        delay(10000)
        println("Time is up!")
    }
}