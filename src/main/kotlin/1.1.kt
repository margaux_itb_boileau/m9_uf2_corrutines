/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 1.1
*/

import kotlinx.coroutines.*

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}
