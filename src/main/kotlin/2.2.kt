/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 2.2
*/

import kotlinx.coroutines.*

fun main() {
    runBlocking {
        printHello2(3)
    }
    println("Finished!")
}

suspend fun printHello2(times:Int) {
    for (i in 1..times) {
        coroutineScope {
            launch {
                println("Message $i: Hello World!")
                delay(1000)
            }
        }
    }
}