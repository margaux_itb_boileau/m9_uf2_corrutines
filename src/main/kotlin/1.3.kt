/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 1.3
*/

import kotlinx.coroutines.*

fun main() {
    println("The main program is started")
    runBlocking {
        doBackground2()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackground2() {
    withContext(Dispatchers.Default){
        launch {
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
    }
}