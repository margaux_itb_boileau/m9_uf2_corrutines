/*
* AUTHOR: Margaux Boileau
* DATE: 2023/04/16
* TITLE: 1.2
*/

import kotlinx.coroutines.*

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackground() {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}